import AWS from 'aws-sdk'

AWS.config.update({ region: 'us-west-2' })

export function sendMessage({address, message, subject}) {
    const params = {
        Destination: {
            ToAddresses: [address]
        },
        Message: {
            Body: {
                Html: {
                    Charset: 'UTF-8',
                    Data: message
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: subject
            }
        },
        Source: 'support@youneedaregistry.com'
    }

    return new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();
}
