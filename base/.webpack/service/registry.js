(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("aws-sdk");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getHistory = exports.addToHistory = exports.testURL = exports.getPurchasedProducts = exports.get = exports.sendPurchasedQuestions = exports.deletePurchaseRecord = exports.markPurchaseConfirmed = exports.markProductPurchased = exports.updateName = exports.updateProductQty = exports.deleteProduct = exports.addProduct = exports.create = undefined;

var _defineProperty2 = __webpack_require__(2);

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _regenerator = __webpack_require__(3);

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(4);

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var create = exports.create = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(event, context, callback) {
        var data, params;
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        params = {
                            TableName: 'registries',
                            Item: {
                                url: data.url,
                                userId: event.requestContext.identity.cognitoIdentityId,
                                name: data.name,
                                description: data.description,
                                products: {}
                            }
                        };
                        _context.prev = 2;
                        _context.next = 5;
                        return dynamoDbLib.call('put', params);

                    case 5:
                        callback(null, (0, _responseLib.success)(params.Item));
                        _context.next = 11;
                        break;

                    case 8:
                        _context.prev = 8;
                        _context.t0 = _context['catch'](2);

                        callback(null, (0, _responseLib.failure)({ status: false }));

                    case 11:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[2, 8]]);
    }));

    return function create(_x, _x2, _x3) {
        return _ref.apply(this, arguments);
    };
}();

var addProduct = exports.addProduct = function () {
    var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(event, context, callback) {
        var data, params;
        return _regenerator2.default.wrap(function _callee2$(_context2) {
            while (1) {
                switch (_context2.prev = _context2.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        params = {
                            TableName: 'registries',
                            Key: {
                                url: data.url
                            },
                            UpdateExpression: 'SET products.#pid = :p',
                            ConditionExpression: 'userId = :user',
                            ExpressionAttributeNames: {
                                '#pid': _uuid2.default.v1()
                            },
                            ExpressionAttributeValues: {
                                ':p': data.product,
                                ':user': event.requestContext.identity.cognitoIdentityId
                            },
                            ReturnValues: 'UPDATED_NEW'
                        };
                        _context2.prev = 2;
                        _context2.next = 5;
                        return dynamoDbLib.call('update', params);

                    case 5:
                        callback(null, (0, _responseLib.success)({ status: true }));
                        _context2.next = 11;
                        break;

                    case 8:
                        _context2.prev = 8;
                        _context2.t0 = _context2['catch'](2);

                        callback(null, (0, _responseLib.failure)({ status: false, err: _context2.t0 }));

                    case 11:
                    case 'end':
                        return _context2.stop();
                }
            }
        }, _callee2, this, [[2, 8]]);
    }));

    return function addProduct(_x4, _x5, _x6) {
        return _ref2.apply(this, arguments);
    };
}();

var deleteProduct = exports.deleteProduct = function () {
    var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(event, context, callback) {
        var data, params;
        return _regenerator2.default.wrap(function _callee3$(_context3) {
            while (1) {
                switch (_context3.prev = _context3.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        params = {
                            TableName: 'registries',
                            Key: {
                                url: data.url
                            },
                            UpdateExpression: 'REMOVE products.#pid',
                            ConditionExpression: 'userId = :user',
                            ExpressionAttributeNames: {
                                '#pid': data.pid
                            },
                            ExpressionAttributeValues: {
                                ':user': event.requestContext.identity.cognitoIdentityId
                            },
                            ReturnValues: 'UPDATED_NEW'
                        };
                        _context3.prev = 2;
                        _context3.next = 5;
                        return dynamoDbLib.call('update', params);

                    case 5:
                        callback(null, (0, _responseLib.success)({ status: true }));
                        _context3.next = 11;
                        break;

                    case 8:
                        _context3.prev = 8;
                        _context3.t0 = _context3['catch'](2);

                        callback(null, (0, _responseLib.failure)({ status: false, err: _context3.t0 }));

                    case 11:
                    case 'end':
                        return _context3.stop();
                }
            }
        }, _callee3, this, [[2, 8]]);
    }));

    return function deleteProduct(_x7, _x8, _x9) {
        return _ref3.apply(this, arguments);
    };
}();

var updateProductQty = exports.updateProductQty = function () {
    var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(event, context, callback) {
        var data, params;
        return _regenerator2.default.wrap(function _callee4$(_context4) {
            while (1) {
                switch (_context4.prev = _context4.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        params = {
                            TableName: 'registries',
                            Key: {
                                url: data.url
                            },
                            UpdateExpression: 'SET products.#pid.qty = :qty',
                            ConditionExpression: 'userId = :user',
                            ExpressionAttributeNames: {
                                '#pid': data.pid
                            },
                            ExpressionAttributeValues: {
                                ':qty': data.qty,
                                ':user': event.requestContext.identity.cognitoIdentityId
                            },
                            ReturnValues: 'UPDATED_NEW'
                        };
                        _context4.prev = 2;
                        _context4.next = 5;
                        return dynamoDbLib.call('update', params);

                    case 5:
                        callback(null, (0, _responseLib.success)({ status: true }));
                        _context4.next = 11;
                        break;

                    case 8:
                        _context4.prev = 8;
                        _context4.t0 = _context4['catch'](2);

                        callback(null, (0, _responseLib.failure)({ status: false, err: _context4.t0 }));

                    case 11:
                    case 'end':
                        return _context4.stop();
                }
            }
        }, _callee4, this, [[2, 8]]);
    }));

    return function updateProductQty(_x10, _x11, _x12) {
        return _ref4.apply(this, arguments);
    };
}();

var updateName = exports.updateName = function () {
    var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(event, context, callback) {
        var data, params;
        return _regenerator2.default.wrap(function _callee5$(_context5) {
            while (1) {
                switch (_context5.prev = _context5.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        params = {
                            TableName: 'registries',
                            Key: {
                                url: data.url
                            },
                            UpdateExpression: 'SET #n = :n, description = :d',
                            ConditionExpression: 'userId = :user',
                            ExpressionAttributeNames: {
                                '#n': 'name'
                            },
                            ExpressionAttributeValues: {
                                ':n': data.name,
                                ':d': data.description,
                                ':user': event.requestContext.identity.cognitoIdentityId
                            },
                            ReturnValues: 'UPDATED_NEW'
                        };
                        _context5.prev = 2;
                        _context5.next = 5;
                        return dynamoDbLib.call('update', params);

                    case 5:
                        callback(null, (0, _responseLib.success)({ status: true }));
                        _context5.next = 11;
                        break;

                    case 8:
                        _context5.prev = 8;
                        _context5.t0 = _context5['catch'](2);

                        callback(null, (0, _responseLib.failure)({ status: false, err: _context5.t0 }));

                    case 11:
                    case 'end':
                        return _context5.stop();
                }
            }
        }, _callee5, this, [[2, 8]]);
    }));

    return function updateName(_x13, _x14, _x15) {
        return _ref5.apply(this, arguments);
    };
}();

var markProductPurchased = exports.markProductPurchased = function () {
    var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(event, context, callback) {
        var data, updateRegistry, createPurchasedRecord, result, product;
        return _regenerator2.default.wrap(function _callee6$(_context6) {
            while (1) {
                switch (_context6.prev = _context6.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        updateRegistry = {
                            TableName: 'registries',
                            Key: {
                                url: data.url
                            },
                            UpdateExpression: 'ADD products.#pid.purchasedTentative :c',
                            ExpressionAttributeNames: {
                                '#pid': data.pid
                            },
                            ExpressionAttributeValues: {
                                ':c': 1
                            },
                            ReturnValues: 'ALL_NEW'
                        };
                        createPurchasedRecord = {
                            TableName: 'purchased-products',
                            Item: {
                                purchasedId: _uuid2.default.v1(),
                                registryUrl: data.url,
                                email: data.email,
                                timestamp: new Date().toISOString(),
                                pid: data.pid,
                                tentative: true
                            }
                        };
                        _context6.prev = 3;
                        _context6.next = 6;
                        return dynamoDbLib.call('update', updateRegistry);

                    case 6:
                        result = _context6.sent;
                        product = result.Attributes.products[data.pid];

                        createPurchasedRecord.Item.product = {};
                        createPurchasedRecord.Item.product.image = product.image;
                        createPurchasedRecord.Item.product.name = product.name;
                        createPurchasedRecord.Item.product.url = product.url;
                        _context6.next = 14;
                        return dynamoDbLib.call('put', createPurchasedRecord);

                    case 14:
                        callback(null, (0, _responseLib.success)({ status: true, purchasedId: createPurchasedRecord.Item.purchasedId }));
                        _context6.next = 20;
                        break;

                    case 17:
                        _context6.prev = 17;
                        _context6.t0 = _context6['catch'](3);

                        callback(null, (0, _responseLib.failure)({ status: false, err: _context6.t0 }));

                    case 20:
                    case 'end':
                        return _context6.stop();
                }
            }
        }, _callee6, this, [[3, 17]]);
    }));

    return function markProductPurchased(_x16, _x17, _x18) {
        return _ref6.apply(this, arguments);
    };
}();

var markPurchaseConfirmed = exports.markPurchaseConfirmed = function () {
    var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7(event, context, callback) {
        var data, getPurchasedRecord, purchased, updateRegistry;
        return _regenerator2.default.wrap(function _callee7$(_context7) {
            while (1) {
                switch (_context7.prev = _context7.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        _context7.prev = 1;
                        getPurchasedRecord = {
                            TableName: 'purchased-products',
                            Key: {
                                purchasedId: data.purchasedId
                            },
                            UpdateExpression: 'SET #tentative = :f',
                            ConditionExpression: '#tentative = :t',
                            ExpressionAttributeNames: {
                                '#tentative': 'tentative'
                            },
                            ExpressionAttributeValues: {
                                ':f': false,
                                ':t': true
                            },
                            ReturnValues: 'ALL_NEW'
                        };
                        _context7.next = 5;
                        return dynamoDbLib.call('update', getPurchasedRecord);

                    case 5:
                        purchased = _context7.sent;

                        purchased = purchased.Attributes;

                        updateRegistry = {
                            TableName: 'registries',
                            Key: {
                                url: purchased.registryUrl
                            },
                            UpdateExpression: 'ADD products.#pid.purchased :c, products.#pid.purchasedTentative :nc',
                            ConditionExpression: 'products.#pid.purchasedTentative > :zero',
                            ExpressionAttributeNames: {
                                '#pid': purchased.pid
                            },
                            ExpressionAttributeValues: {
                                ':c': 1,
                                ':nc': -1,
                                ':zero': 0
                            }
                        };
                        _context7.next = 10;
                        return dynamoDbLib.call('update', updateRegistry);

                    case 10:
                        callback(null, (0, _responseLib.success)({ status: true }));
                        _context7.next = 16;
                        break;

                    case 13:
                        _context7.prev = 13;
                        _context7.t0 = _context7['catch'](1);

                        callback(null, (0, _responseLib.failure)({ status: false, err: _context7.t0 }));

                    case 16:
                    case 'end':
                        return _context7.stop();
                }
            }
        }, _callee7, this, [[1, 13]]);
    }));

    return function markPurchaseConfirmed(_x19, _x20, _x21) {
        return _ref7.apply(this, arguments);
    };
}();

var deletePurchaseRecord = exports.deletePurchaseRecord = function () {
    var _ref8 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8(event, context, callback) {
        var data, getPurchasedRecord, purchased, updateRegistry;
        return _regenerator2.default.wrap(function _callee8$(_context8) {
            while (1) {
                switch (_context8.prev = _context8.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        _context8.prev = 1;
                        getPurchasedRecord = {
                            TableName: 'purchased-products',
                            Key: {
                                purchasedId: data.purchasedId
                            },
                            ReturnValues: 'ALL_OLD'
                        };
                        _context8.next = 5;
                        return dynamoDbLib.call('delete', getPurchasedRecord);

                    case 5:
                        purchased = _context8.sent;

                        purchased = purchased.Attributes;

                        updateRegistry = {
                            TableName: 'registries',
                            Key: {
                                url: purchased.registryUrl
                            },
                            UpdateExpression: 'ADD products.#pid.#purchased :nc',
                            ConditionExpression: 'products.#pid.#purchased > :zero',
                            ExpressionAttributeNames: {
                                '#pid': purchased.pid,
                                '#purchased': purchased.tentative ? 'purchasedTentative' : 'purchased'
                            },
                            ExpressionAttributeValues: {
                                ':nc': -1,
                                ':zero': 0
                            }
                        };
                        _context8.next = 10;
                        return dynamoDbLib.call('update', updateRegistry);

                    case 10:
                        callback(null, (0, _responseLib.success)({ status: true }));
                        _context8.next = 16;
                        break;

                    case 13:
                        _context8.prev = 13;
                        _context8.t0 = _context8['catch'](1);

                        callback(null, (0, _responseLib.failure)({ status: false, err: _context8.t0 }));

                    case 16:
                    case 'end':
                        return _context8.stop();
                }
            }
        }, _callee8, this, [[1, 13]]);
    }));

    return function deletePurchaseRecord(_x22, _x23, _x24) {
        return _ref8.apply(this, arguments);
    };
}();

var sendPurchasedQuestions = exports.sendPurchasedQuestions = function () {
    var _ref9 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee9(event, context, callback) {
        var currentDate, dateFilter, params, result, i, record;
        return _regenerator2.default.wrap(function _callee9$(_context9) {
            while (1) {
                switch (_context9.prev = _context9.next) {
                    case 0:
                        currentDate = new Date();
                        dateFilter = currentDate;

                        dateFilter.setMinutes(currentDate.getMinutes() - 120);

                        params = {
                            TableName: 'purchased-products',
                            FilterExpression: '#tentative = :isTentative AND #timestamp < :time',
                            ExpressionAttributeNames: {
                                '#tentative': 'tentative',
                                '#timestamp': 'timestamp'
                            },
                            ExpressionAttributeValues: {
                                ':time': dateFilter.toISOString(),
                                ':isTentative': true
                            }
                        };
                        _context9.prev = 4;
                        _context9.next = 7;
                        return dynamoDbLib.call('scan', params);

                    case 7:
                        result = _context9.sent;
                        i = 0;

                    case 9:
                        if (!(i < result.Items.length)) {
                            _context9.next = 17;
                            break;
                        }

                        record = result.Items[i];

                        console.log('sending...', record);
                        _context9.next = 14;
                        return sesLib.sendMessage({
                            address: record.email,
                            message: '<a href="https://youneedaregistry.com/registry/mark-product/' + record.purchasedId + '/?mark=true">I bought this!</a>',
                            subject: 'Did you buy this thing?'
                        });

                    case 14:
                        i++;
                        _context9.next = 9;
                        break;

                    case 17:
                        callback(null, (0, _responseLib.success)({ status: true }));
                        _context9.next = 23;
                        break;

                    case 20:
                        _context9.prev = 20;
                        _context9.t0 = _context9['catch'](4);

                        callback(null, (0, _responseLib.failure)({ status: false, err: _context9.t0 }));

                    case 23:
                    case 'end':
                        return _context9.stop();
                }
            }
        }, _callee9, this, [[4, 20]]);
    }));

    return function sendPurchasedQuestions(_x25, _x26, _x27) {
        return _ref9.apply(this, arguments);
    };
}();

var get = exports.get = function () {
    var _ref10 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee10(event, context, callback) {
        var params, result;
        return _regenerator2.default.wrap(function _callee10$(_context10) {
            while (1) {
                switch (_context10.prev = _context10.next) {
                    case 0:
                        params = {
                            TableName: 'registries',
                            Key: {
                                url: event.pathParameters.url
                            }
                        };
                        _context10.prev = 1;
                        _context10.next = 4;
                        return dynamoDbLib.call('get', params);

                    case 4:
                        result = _context10.sent;

                        if (result.Item) {
                            callback(null, (0, _responseLib.success)(result.Item));
                        } else {
                            callback(null, (0, _responseLib.failure)({ status: false, error: 'Registry not found!' }));
                        }
                        _context10.next = 11;
                        break;

                    case 8:
                        _context10.prev = 8;
                        _context10.t0 = _context10['catch'](1);

                        callback(null, (0, _responseLib.failure)({ status: false }));

                    case 11:
                    case 'end':
                        return _context10.stop();
                }
            }
        }, _callee10, this, [[1, 8]]);
    }));

    return function get(_x28, _x29, _x30) {
        return _ref10.apply(this, arguments);
    };
}();

var getPurchasedProducts = exports.getPurchasedProducts = function () {
    var _ref11 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee11(event, context, callback) {
        var data, params, result;
        return _regenerator2.default.wrap(function _callee11$(_context11) {
            while (1) {
                switch (_context11.prev = _context11.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        params = {
                            TableName: 'purchased-products',
                            FilterExpression: 'email = :email' + (data.url ? ' AND registryUrl = :url' : ''),
                            ExpressionAttributeValues: {
                                ':email': data.email
                            }
                        };


                        if (data.url) {
                            params.ExpressionAttributeValues[':url'] = data.url;
                        }

                        _context11.prev = 3;
                        _context11.next = 6;
                        return dynamoDbLib.call('scan', params);

                    case 6:
                        result = _context11.sent;

                        callback(null, (0, _responseLib.success)({ status: true, records: result.Items }));
                        _context11.next = 13;
                        break;

                    case 10:
                        _context11.prev = 10;
                        _context11.t0 = _context11['catch'](3);

                        callback(null, (0, _responseLib.failure)({ status: false, err: _context11.t0 }));

                    case 13:
                    case 'end':
                        return _context11.stop();
                }
            }
        }, _callee11, this, [[3, 10]]);
    }));

    return function getPurchasedProducts(_x31, _x32, _x33) {
        return _ref11.apply(this, arguments);
    };
}();

var testURL = exports.testURL = function () {
    var _ref12 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee12(event, context, callback) {
        var data, params, result;
        return _regenerator2.default.wrap(function _callee12$(_context12) {
            while (1) {
                switch (_context12.prev = _context12.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        params = {
                            TableName: 'registries',
                            Key: {
                                url: data.url
                            }
                        };
                        _context12.prev = 2;
                        _context12.next = 5;
                        return dynamoDbLib.call('get', params);

                    case 5:
                        result = _context12.sent;

                        callback(null, (0, _responseLib.success)({ available: !result.Item }));
                        _context12.next = 12;
                        break;

                    case 9:
                        _context12.prev = 9;
                        _context12.t0 = _context12['catch'](2);

                        callback(null, (0, _responseLib.failure)({ status: false }));

                    case 12:
                    case 'end':
                        return _context12.stop();
                }
            }
        }, _callee12, this, [[2, 9]]);
    }));

    return function testURL(_x34, _x35, _x36) {
        return _ref12.apply(this, arguments);
    };
}();

var addToHistory = exports.addToHistory = function () {
    var _ref13 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee13(event, context, callback) {
        var data, params, newRecordParams;
        return _regenerator2.default.wrap(function _callee13$(_context13) {
            while (1) {
                switch (_context13.prev = _context13.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        params = {
                            TableName: 'registry-history',
                            Key: {
                                userId: event.requestContext.identity.cognitoIdentityId
                            },
                            UpdateExpression: 'SET registries.#url = :t',
                            ExpressionAttributeNames: {
                                '#url': data.url
                            },
                            ExpressionAttributeValues: {
                                ':t': data.name
                            }
                        };
                        _context13.prev = 2;
                        _context13.next = 5;
                        return dynamoDbLib.call('update', params);

                    case 5:
                        callback(null, (0, _responseLib.success)({ status: true }));
                        _context13.next = 20;
                        break;

                    case 8:
                        _context13.prev = 8;
                        _context13.t0 = _context13['catch'](2);
                        newRecordParams = {
                            TableName: 'registry-history',
                            Item: {
                                userId: event.requestContext.identity.cognitoIdentityId,
                                registries: (0, _defineProperty3.default)({}, data.url, data.name)
                            }
                        };
                        _context13.prev = 11;
                        _context13.next = 14;
                        return dynamoDbLib.call('put', newRecordParams);

                    case 14:
                        callback(null, (0, _responseLib.success)({ status: true }));
                        _context13.next = 20;
                        break;

                    case 17:
                        _context13.prev = 17;
                        _context13.t1 = _context13['catch'](11);

                        callback(null, (0, _responseLib.failure)({ status: false }));

                    case 20:
                    case 'end':
                        return _context13.stop();
                }
            }
        }, _callee13, this, [[2, 8], [11, 17]]);
    }));

    return function addToHistory(_x37, _x38, _x39) {
        return _ref13.apply(this, arguments);
    };
}();

var getHistory = exports.getHistory = function () {
    var _ref14 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee14(event, context, callback) {
        var data, params, result;
        return _regenerator2.default.wrap(function _callee14$(_context14) {
            while (1) {
                switch (_context14.prev = _context14.next) {
                    case 0:
                        data = JSON.parse(event.body);
                        params = {
                            TableName: 'registry-history',
                            Key: {
                                userId: event.requestContext.identity.cognitoIdentityId
                            }
                        };
                        _context14.prev = 2;
                        _context14.next = 5;
                        return dynamoDbLib.call('get', params);

                    case 5:
                        result = _context14.sent;

                        callback(null, (0, _responseLib.success)({ status: true, item: result.Item }));
                        _context14.next = 12;
                        break;

                    case 9:
                        _context14.prev = 9;
                        _context14.t0 = _context14['catch'](2);

                        callback(null, (0, _responseLib.failure)({ status: false }));

                    case 12:
                    case 'end':
                        return _context14.stop();
                }
            }
        }, _callee14, this, [[2, 9]]);
    }));

    return function getHistory(_x40, _x41, _x42) {
        return _ref14.apply(this, arguments);
    };
}();

var _uuid = __webpack_require__(5);

var _uuid2 = _interopRequireDefault(_uuid);

var _dynamodbLib = __webpack_require__(6);

var dynamoDbLib = _interopRequireWildcard(_dynamodbLib);

var _sesLib = __webpack_require__(7);

var sesLib = _interopRequireWildcard(_sesLib);

var _responseLib = __webpack_require__(8);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/defineProperty");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/regenerator");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/asyncToGenerator");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("uuid");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.call = call;

var _awsSdk = __webpack_require__(0);

var _awsSdk2 = _interopRequireDefault(_awsSdk);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_awsSdk2.default.config.update({ region: 'us-west-2' });

function call(action, params) {
    var dynamoDb = new _awsSdk2.default.DynamoDB.DocumentClient();

    return dynamoDb[action](params).promise();
}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.sendMessage = sendMessage;

var _awsSdk = __webpack_require__(0);

var _awsSdk2 = _interopRequireDefault(_awsSdk);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_awsSdk2.default.config.update({ region: 'us-west-2' });

function sendMessage(_ref) {
    var address = _ref.address,
        message = _ref.message,
        subject = _ref.subject;

    var params = {
        Destination: {
            ToAddresses: [address]
        },
        Message: {
            Body: {
                Html: {
                    Charset: 'UTF-8',
                    Data: message
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: subject
            }
        },
        Source: 'support@youneedaregistry.com'
    };

    return new _awsSdk2.default.SES({ apiVersion: '2010-12-01' }).sendEmail(params).promise();
}

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = __webpack_require__(9);

var _stringify2 = _interopRequireDefault(_stringify);

exports.success = success;
exports.failure = failure;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function success(body) {
    return buildResponse(200, body);
}

function failure(body) {
    return buildResponse(500, body);
}

function buildResponse(statusCode, body) {
    return {
        statusCode: statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body: (0, _stringify2.default)(body)
    };
}

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/core-js/json/stringify");

/***/ })
/******/ ])));