import { success, failure } from '../libs/response-lib'
import productApi from 'amazon-product-api'
import WAE from 'web-auto-extractor'
import request from 'request-promise'

const getClient = () => {
    return productApi.createClient({
        awsId: 'AKIAJOQNWTADG5YQ2RQA',
        awsSecret: process.env.AWS_SECRET,
        awsTag: 'youneedaregis-20'
    })
}

export async function itemLookup (event, context, callback) {
    const data = JSON.parse(event.body)

    if (data.url) {
        try {
            const html = await request({
                uri: data.url,
                headers: {
                    'User-Agent': 'Mozilla/5.0 (compatible; YNARBot/1.0; +http://www.youneedaregistry.com)'
                }
            })
            const metadata = await WAE().parse(html)
            callback(null, success(metadata.microdata.Product))
        } catch (e) {
            callback(null, failure(e))
        }
    } else {
        try {
            const results = await getClient().itemLookup({
                itemId: data.itemId,
                responseGroup: 'ItemAttributes,Images,Offers,OfferSummary'
            })
            callback(null, success(results))
        } catch (e) {
            callback(null, failure(e))
        }
    }
}
