import uuid from 'uuid'
import * as dynamoDbLib from '../libs/dynamodb-lib'
import { success, failure } from '../libs/response-lib'

export async function main(event, context, callback) {
    const data = JSON.parse(event.body)

    const params = {
        TableName: 'products',
        Item: {
            productId: uuid.v1(),
            link: data.link,
            image: data.image,
            name: data.name,
            description: data.description,
            occasions: data.occasions
        }
    }

    try {
        await dynamoDbLib.call('put', params)
        callback(null, success(params.Item))
    } catch (e) {
        callback(null, failure({ status: false}))
    }
}