import uuid from 'uuid'
import * as dynamoDbLib from '../libs/dynamodb-lib'
import * as sesLib from '../libs/ses-lib'
import { success, failure } from '../libs/response-lib'

export async function create (event, context, callback) {
    const data = JSON.parse(event.body)

    const params = {
        TableName: 'registries',
        Item: {
            url: data.url,
            userId: event.requestContext.identity.cognitoIdentityId,
            name: data.name,
            description: data.description,
            products: {}
        }
    }

    try {
        await dynamoDbLib.call('put', params)
        callback(null, success(params.Item))
    } catch (e) {
        callback(null, failure({ status: false}))
    }
}

export async function addProduct (event, context, callback) {
    const data = JSON.parse(event.body)

    const params = {
        TableName: 'registries',
        Key: {
            url: data.url
        },
        UpdateExpression: 'SET products.#pid = :p',
        ConditionExpression: 'userId = :user',
        ExpressionAttributeNames: {
            '#pid': uuid.v1()
        },
        ExpressionAttributeValues: {
            ':p': data.product,
            ':user': event.requestContext.identity.cognitoIdentityId
        },
        ReturnValues: 'UPDATED_NEW'
    }

    try {
        await dynamoDbLib.call('update', params)
        callback(null, success({ status: true }))
    } catch (e) {
        callback(null, failure({ status: false, err: e }))
    }
}

export async function deleteProduct (event, context, callback) {
    const data = JSON.parse(event.body)

    const params = {
        TableName: 'registries',
        Key: {
            url: data.url
        },
        UpdateExpression: 'REMOVE products.#pid',
        ConditionExpression: 'userId = :user',
        ExpressionAttributeNames: {
            '#pid': data.pid
        },
        ExpressionAttributeValues: {
            ':user': event.requestContext.identity.cognitoIdentityId
        },
        ReturnValues: 'UPDATED_NEW'
    }

    try {
        await dynamoDbLib.call('update', params)
        callback(null, success({ status: true }))
    } catch (e) {
        callback(null, failure({ status: false, err: e }))
    }
}

export async function updateProductQty (event, context, callback) {
    const data = JSON.parse(event.body)

    const params = {
        TableName: 'registries',
        Key: {
            url: data.url
        },
        UpdateExpression: 'SET products.#pid.qty = :qty',
        ConditionExpression: 'userId = :user',
        ExpressionAttributeNames: {
            '#pid': data.pid
        },
        ExpressionAttributeValues: {
            ':qty': data.qty,
            ':user': event.requestContext.identity.cognitoIdentityId
        },
        ReturnValues: 'UPDATED_NEW'
    }

    try {
        await dynamoDbLib.call('update', params)
        callback(null, success({ status: true }))
    } catch (e) {
        callback(null, failure({ status: false, err: e }))
    }
}

export async function updateName (event, context, callback) {
    const data = JSON.parse(event.body)

    const params = {
        TableName: 'registries',
        Key: {
            url: data.url
        },
        UpdateExpression: 'SET #n = :n, description = :d',
        ConditionExpression: 'userId = :user',
        ExpressionAttributeNames: {
            '#n': 'name'
        },
        ExpressionAttributeValues: {
            ':n': data.name,
            ':d': data.description,
            ':user': event.requestContext.identity.cognitoIdentityId
        },
        ReturnValues: 'UPDATED_NEW'
    }

    try {
        await dynamoDbLib.call('update', params)
        callback(null, success({ status: true }))
    } catch (e) {
        callback(null, failure({ status: false, err: e }))
    }
}

export async function markProductPurchased (event, context, callback) {
    const data = JSON.parse(event.body)

    const updateRegistry = {
        TableName: 'registries',
        Key: {
            url: data.url
        },
        UpdateExpression: 'ADD products.#pid.purchasedTentative :c',
        ExpressionAttributeNames: {
            '#pid': data.pid
        },
        ExpressionAttributeValues: {
            ':c': 1
        },
        ReturnValues: 'ALL_NEW'
    }

    const createPurchasedRecord = {
        TableName: 'purchased-products',
        Item: {
            purchasedId: uuid.v1(),
            registryUrl: data.url,
            email: data.email,
            timestamp: new Date().toISOString(),
            pid: data.pid,
            tentative: true
        }
    }

    try {
        const result = await dynamoDbLib.call('update', updateRegistry)
        const product = result.Attributes.products[data.pid]
        createPurchasedRecord.Item.product = {}
        createPurchasedRecord.Item.product.image = product.image
        createPurchasedRecord.Item.product.name = product.name
        createPurchasedRecord.Item.product.url = product.url
        await dynamoDbLib.call('put', createPurchasedRecord)
        callback(null, success({ status: true, purchasedId: createPurchasedRecord.Item.purchasedId }))
    } catch (e) {
        callback(null, failure({ status: false, err: e }))
    }
}

export async function markPurchaseConfirmed (event, context, callback) {
    const data = JSON.parse(event.body)

    try {
        const getPurchasedRecord = {
            TableName: 'purchased-products',
            Key: {
                purchasedId: data.purchasedId
            },
            UpdateExpression: 'SET #tentative = :f',
            ConditionExpression: '#tentative = :t',
            ExpressionAttributeNames: {
                '#tentative': 'tentative'
            },
            ExpressionAttributeValues: {
                ':f': false,
                ':t': true
            },
            ReturnValues: 'ALL_NEW'
        }

        let purchased = await dynamoDbLib.call('update', getPurchasedRecord)
        purchased = purchased.Attributes

        const updateRegistry = {
            TableName: 'registries',
            Key: {
                url: purchased.registryUrl
            },
            UpdateExpression: 'ADD products.#pid.purchased :c, products.#pid.purchasedTentative :nc',
            ConditionExpression: 'products.#pid.purchasedTentative > :zero',
            ExpressionAttributeNames: {
                '#pid': purchased.pid
            },
            ExpressionAttributeValues: {
                ':c': 1,
                ':nc': -1,
                ':zero': 0
            }
        }

        await dynamoDbLib.call('update', updateRegistry)
        callback(null, success({ status: true }))
    } catch (e) {
        callback(null, failure({ status: false, err: e }))
    }
}

export async function deletePurchaseRecord (event, context, callback) {
    const data = JSON.parse(event.body)

    try {
        const getPurchasedRecord = {
            TableName: 'purchased-products',
            Key: {
                purchasedId: data.purchasedId
            },
            ReturnValues: 'ALL_OLD'
        }

        let purchased = await dynamoDbLib.call('delete', getPurchasedRecord)
        purchased = purchased.Attributes

        const updateRegistry = {
            TableName: 'registries',
            Key: {
                url: purchased.registryUrl
            },
            UpdateExpression: 'ADD products.#pid.#purchased :nc',
            ConditionExpression: 'products.#pid.#purchased > :zero',
            ExpressionAttributeNames: {
                '#pid': purchased.pid,
                '#purchased': purchased.tentative ? 'purchasedTentative' : 'purchased'
            },
            ExpressionAttributeValues: {
                ':nc': -1,
                ':zero': 0
            }
        }

        await dynamoDbLib.call('update', updateRegistry)
        callback(null, success({ status: true }))
    } catch (e) {
        callback(null, failure({ status: false, err: e }))
    }
}

export async function sendPurchasedQuestions (event, context, callback) {
    const currentDate = new Date()
    const dateFilter = currentDate
    dateFilter.setMinutes(currentDate.getMinutes() - 120)

    const params = {
        TableName: 'purchased-products',
        FilterExpression: '#tentative = :isTentative AND #timestamp < :time',
        ExpressionAttributeNames: {
            '#tentative': 'tentative',
            '#timestamp': 'timestamp'
        },
        ExpressionAttributeValues: {
            ':time': dateFilter.toISOString(),
            ':isTentative': true
        }
    }

    try {
        const result = await dynamoDbLib.call('scan', params)
        for (let i = 0; i < result.Items.length; i++) {
            let record = result.Items[i]
            console.log('sending...', record)
            await sesLib.sendMessage({
                address: record.email,
                message: `<a href="https://youneedaregistry.com/registry/mark-product/${record.purchasedId}/?mark=true">I bought this!</a>`,
                subject: 'Did you buy this thing?'
            })
        }
        callback(null, success({ status: true }))
    } catch (e) {
        callback(null, failure({ status: false, err: e }))
    }
}

export async function get (event, context, callback) {
    const params = {
        TableName: 'registries',
        Key: {
            url: event.pathParameters.url
        }
    }

    try {
        const result = await dynamoDbLib.call('get', params)
        if (result.Item) {
            callback(null, success(result.Item))
        } else {
            callback(null, failure({ status: false, error: 'Registry not found!' }))
        }
    } catch (e) {
        callback(null, failure({ status: false }))
    }
}

export async function getPurchasedProducts (event, context, callback) {
    const data = JSON.parse(event.body)

    const params = {
        TableName: 'purchased-products',
        FilterExpression: `email = :email${data.url ? ' AND registryUrl = :url' : ''}`,
        ExpressionAttributeValues: {
            ':email': data.email
        }
    }

    if (data.url) {
        params.ExpressionAttributeValues[':url'] = data.url
    }

    try {
        const result = await dynamoDbLib.call('scan', params)
        callback(null, success({ status: true, records: result.Items }))
    } catch (e) {
        callback(null, failure({ status: false, err: e }))
    }
}

export async function testURL (event, context, callback) {
    const data = JSON.parse(event.body)

    const params = {
        TableName: 'registries',
        Key: {
            url: data.url
        }
    }

    try {
        const result = await dynamoDbLib.call('get', params)
        callback(null, success({available: !result.Item}))
    } catch (e) {
        callback(null, failure({ status: false }))
    }
}

export async function addToHistory (event, context, callback) {
    const data = JSON.parse(event.body)

    const params = {
        TableName: 'registry-history',
        Key: {
            userId: event.requestContext.identity.cognitoIdentityId
        },
        UpdateExpression: 'SET registries.#url = :t',
        ExpressionAttributeNames: {
            '#url': data.url
        },
        ExpressionAttributeValues: {
            ':t': data.name
        }
    }

    try {
        await dynamoDbLib.call('update', params)
        callback(null, success({status: true}))
    } catch (e) {
        const newRecordParams = {
            TableName: 'registry-history',
            Item: {
                userId: event.requestContext.identity.cognitoIdentityId,
                registries: {[data.url]: data.name}
            }
        }

        try {
            await dynamoDbLib.call('put', newRecordParams)
            callback(null, success({status: true}))
        } catch (e) {
            callback(null, failure({ status: false }))
        }
    }
}

export async function getHistory (event, context, callback) {
    const data = JSON.parse(event.body)

    const params = {
        TableName: 'registry-history',
        Key: {
            userId: event.requestContext.identity.cognitoIdentityId
        }
    }

    try {
        const result = await dynamoDbLib.call('get', params)
        callback(null, success({status: true, item: result.Item}))
    } catch (e) {
        callback(null, failure({status: false}))
    }
}